import {Component, OnDestroy, OnInit} from '@angular/core';
import {StorageService} from '../services/storage.service';
import {FormControl, FormGroup} from '@angular/forms';
import {GeocodingService} from '../services/geocoding.service';
import {Subscription} from 'rxjs';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AlertController, LoadingController} from '@ionic/angular';

declare var google;

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnDestroy {
    listDeliverPositions: any[];
    listPlaces: any[];
    search: string;
    currentPosition: any;

    formSearch: FormGroup;

    private autocompleteService: any;
    private subscriptions: Subscription;

    constructor(private storage: StorageService,
                private gCodeService: GeocodingService,
                private geolocation: Geolocation,
                private alertController: AlertController,
                public loadingController: LoadingController) {
        this.listDeliverPositions = [];
        this.listPlaces = [];
        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.formSearch = new FormGroup({
            name: new FormControl('')
        });
        this.subscriptions = new Subscription();
    }

    ionViewWillEnter(): void {
        this.geolocation.getCurrentPosition().then(position => {
            this.subscriptions.add(
                this.gCodeService.getGeocodeFromLocationLatLng({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }).subscribe(async response => {
                    if (response.status === 'OK') {
                        this.currentPosition = {
                            address: response.results[0].formatted_address,
                            postalCode: response.results[0].address_components.find(addressComponent => {
                                const postalCode = addressComponent.types.find(x => {
                                    return x === 'postal_code';
                                });
                                if (postalCode) {
                                    return addressComponent;
                                }

                            }).long_name || '',
                            location: {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            }
                        };
                        this.storage.getDeliverArrayPoints().then(deliveryList => {
                            if (deliveryList) {
                                this.listDeliverPositions = deliveryList;
                                this.listDeliverPositions.forEach(deliveryPoint => {
                                    if (!deliveryPoint.distance && !deliveryPoint.duration) {
                                        const serviceDistance = new google.maps.DistanceMatrixService();
                                        serviceDistance.getDistanceMatrix({
                                            origins: [{lat: this.currentPosition.location.lat, lng: this.currentPosition.location.lng}],
                                            destinations: [deliveryPoint.geometry.location],
                                            travelMode: 'DRIVING',
                                            unitSystem: google.maps.UnitSystem.METRIC,
                                            avoidHighways: true,
                                            avoidTolls: false,
                                        }, async (result, status) => {
                                            if (status === 'OK') {
                                                deliveryPoint.distance = result.rows[0].elements[0].distance;
                                                deliveryPoint.duration = result.rows[0].elements[0].duration;
                                            } else {
                                                const alert = await this.alertController.create({
                                                    message: 'Error metrix service'
                                                });
                                                await alert.present();
                                            }
                                        });
                                    }
                                    setTimeout(() => {
                                        this.listDeliverPositions.sort((a, b) => {
                                            if (a.distance.value < b.distance.value) {
                                                return -1;
                                            } else if (a.distance.value > b.distance.value) {
                                                return 1;
                                            }
                                            return 0;
                                        });
                                        this.storage.setDeliveryArrayPoints(this.listDeliverPositions);
                                    }, 2000);
                                });
                            }
                        });
                    }
                })
            );
        })
            .catch(async err => {
                const alert = await this.alertController.create({
                    message: err,
                    buttons: [
                        {
                            text: 'ok',
                            handler: async () => {
                                await alert.dismiss();
                            }
                        }
                    ]
                });
                await alert.present();
            });
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }


    inputChange(container, value): void {
        this.search = value;

        if ((container as HTMLElement).classList.contains('hide')) {
            (container as HTMLElement).classList.remove('hide');
        }

        this.autocompleteService.getPlacePredictions({input: this.search}, (predictions) => {
            this.listPlaces = predictions;
        });
    }


    async clickAddDeliveryPositionToList() {
        const loading = await this.loadingController.create({
            spinner: 'lines',
            message: 'please wait'
        });
        await loading.present();
        this.subscriptions.add(
            this.gCodeService.getGeocode(this.search).subscribe(response => {
                const serviceDistance = new google.maps.DistanceMatrixService();
                if (response.status === 'OK') {
                    serviceDistance.getDistanceMatrix({
                        origins: [{lat: this.currentPosition.location.lat, lng: this.currentPosition.location.lng}],
                        destinations: [response.results[0].geometry.location],
                        travelMode: 'DRIVING',
                        unitSystem: google.maps.UnitSystem.METRIC,
                        avoidHighways: true,
                        avoidTolls: false,
                    }, async (result, status) => {
                        if (status === 'OK') {
                            this.listDeliverPositions.push({
                                id: this.listDeliverPositions.length,
                                plce_id: response.results[0].place_id,
                                address: response.results[0].formatted_address,
                                geometry: response.results[0].geometry,
                                distance: result.rows[0].elements[0].distance,
                                duration: result.rows[0].elements[0].duration,
                                isComplete: false
                            });
                            this.listDeliverPositions.sort((a, b) => {
                                if (a.distance.value < b.distance.value) {
                                    return -1;
                                } else if (a.distance.value > b.distance.value) {
                                    return 1;
                                }
                                return 0;
                            });
                            this.storage.setDeliveryArrayPoints(this.listDeliverPositions);
                            await loading.dismiss();
                        }
                    });
                }
            })
        );
        const input = document.getElementById('searchPlace');
        if (input) {
            (input as HTMLInputElement).value = '';
        }
        this.search = '';
        this.listPlaces = [];
    }

    clickRemovePosition(id: string) {
        this.listDeliverPositions = this.listDeliverPositions.filter(x => x.plce_id !== id);
        this.storage.setDeliveryArrayPoints(this.listDeliverPositions);
    }

    clickSelectItem(item, container): void {
        this.formSearch.patchValue({
            name: item
        });
        this.listPlaces = [];
        this.search = item;
        (container as HTMLElement).classList.add('hide');
    }

}
