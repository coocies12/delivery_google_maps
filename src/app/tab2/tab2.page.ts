import {AfterContentInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {StorageService} from '../services/storage.service';
import {BehaviorSubject, Subscription} from 'rxjs';
import {isLineBreak} from 'codelyzer/angular/sourceMappingVisitor';

declare var google;

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, OnDestroy, AfterContentInit {
    map: any;
    marker: any;
    mapElement: any;
    directionsService: any;
    directionsDisplay: any;

    private myCurrentPosition: BehaviorSubject<{
        lat: number;
        long: number;
    }>;
    private listDeliveryDistance: any[];
    private subscription: Subscription;
    private Loading: HTMLIonLoadingElement;
    private interval;
    private serviceDistance;
    private deliveryPoint: any;

    constructor(private geolocation: Geolocation,
                private alertController: AlertController,
                private storage: StorageService,
                public loadingController: LoadingController,
                private toastController: ToastController) {
        this.listDeliveryDistance = [];
        this.subscription = new Subscription();
        this.myCurrentPosition = new BehaviorSubject<{ lat: number, long: number }>(null);
    }

    // start first
    ngOnInit(): void {
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.serviceDistance = new google.maps.DistanceMatrixService();
    }

    ionViewWillEnter() {
        this.storage.getDeliverArrayPoints().then((listDelivery: any[]) => {
            this.listDeliveryDistance = listDelivery;
        });
        this.geolocation.getCurrentPosition().then(position => {
            this.presentLoading();
            this.myCurrentPosition.next(
                {
                    lat: position.coords.latitude,
                    long: position.coords.longitude
                });
        });
        this.mapElement = (document.getElementById('mapElement') as HTMLElement);
        this.subscription.add(
            this.myCurrentPosition.subscribe(position => {
                if (position) {
                    this.map = new google.maps.Map(
                        this.mapElement,
                        {
                            center: {lat: position.lat, lng: position.long},
                            zoom: 15
                        }
                    );
                    this.directionsDisplay.setMap(this.map);
                }
            })
        );
    }

    // start second
    ngAfterContentInit(): void {
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    async goTo() {
        try {
            if (this.deliveryPoint) {
                this.geolocation.getCurrentPosition().then(position => {
                    if (position && position.coords) {
                        this.serviceDistance.getDistanceMatrix({
                            origins: [{lat: position.coords.latitude, lng: position.coords.longitude}],
                            destinations: [this.deliveryPoint.geometry.location],
                            travelMode: 'DRIVING',
                            unitSystem: google.maps.UnitSystem.METRIC,
                            avoidHighways: true,
                            avoidTolls: false,
                        }, async (metrixResult, metrixStatus) => {
                            if (metrixStatus === 'OK') {
                                if (metrixResult.rows[0].elements[0].distance.value >= 20) {
                                    this.directionsService.route({
                                        origin: {lat: position.coords.latitude, lng: position.coords.longitude},
                                        destination: this.deliveryPoint.geometry.location,
                                        travelMode: 'DRIVING',
                                        optimizeWaypoints: true,
                                        provideRouteAlternatives: false,
                                        avoidTolls: true,
                                    }, async (result, status) => {
                                        if (status === 'OK') {
                                            this.directionsDisplay.setDirections(result);
                                            this.directionsDisplay.setMap(this.map);
                                        }
                                    });
                                } else {
                                    clearInterval(this.interval);
                                    const alert = await this.alertController.create({
                                        message: 'delivery point complete'
                                    });
                                    await alert.present();
                                    this.listDeliveryDistance = this.listDeliveryDistance.filter(x => x.plce_id !== this.deliveryPoint.plce_id);
                                    this.storage.setDeliveryArrayPoints(this.listDeliveryDistance);
                                }
                            }
                        });
                    }

                });
            }
        } catch (e) {
            const alert = await this.alertController.create({
                message: 'error go to'
            });
            await alert.present();
        }
    }

    async clickGoToDelivery() {
        try {
            for (const deliveryPoint of this.listDeliveryDistance) {
                if (!deliveryPoint.isComplete) {
                    this.deliveryPoint = deliveryPoint;
                    this.interval = setInterval(() => {
                        this.goTo();
                    }, 2500);
                    break;
                }
            }
        } catch (e) {
            const alert = await this.alertController.create({
                message: 'error click to delivery ' + e,
            });
            await alert.present();
        }
    }

    async presentLoading() {
        this.Loading = await this.loadingController.create({
            message: 'Please wait',
            spinner: 'lines',
            duration: 1000
        });
        await this.Loading.present();
    }

}
