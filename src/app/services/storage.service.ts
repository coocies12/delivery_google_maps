import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private storage: Storage) {
    }

    setDeliveryArrayPoints(value: any[]): Promise<any> {
        return this.storage.set('list', value);
    }

    getDeliverArrayPoints(): Promise<any> {
        return this.storage.get('list').then(value => {
            return value;
        })
    }

    deleteDeliveryList(): Promise<any> {
        return this.storage.remove('list');
    }
}
