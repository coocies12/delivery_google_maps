import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GeocodingService {
    private readonly KEY = 'AIzaSyClgsn2U-430WdxX5_4lKbpTCVB_YWRDB8';

    constructor(private http: HttpClient) {
    }

    getGeocode(address: string): Observable<any> {
        return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${this.KEY}`)
            .pipe(map(res => res));
    }

    getGeocodeFromLocationLatLng(location): Observable<any> {
        return this.http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.lat}, ${location.lng}&key=${this.KEY}`)
            .pipe(map(res => res));
    }
}
